package care.ai.apptest

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.hivemq.client.mqtt.MqttClient
import com.hivemq.client.mqtt.MqttGlobalPublishFilter
import com.hivemq.client.mqtt.datatypes.MqttQos
import org.json.JSONObject
import kotlin.text.Charsets.UTF_8

class MainActivity : AppCompatActivity() {

    private val client = MqttClient.builder()
        .useMqttVersion3()
        .identifier(IDENTIFIER)
        .serverHost(HOST)
        .serverPort(8883)
        .simpleAuth()
            .username(USER)
            .password(UTF_8.encode(PASS))
            .applySimpleAuth()
        .sslWithDefaultConfig()
        .buildBlocking()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        client.toAsync().connectWith()
            .simpleAuth()
                .username(USER)
                .password(UTF_8.encode(PASS))
                .applySimpleAuth()
            .send()
            .whenComplete { connAck, throwable ->
                if (throwable != null) {
                    Log.e(MainActivity::class.java.simpleName,
                        "Failed to connect to MQTT broker.",
                        throwable)
                    return@whenComplete
                }
                Log.d(MainActivity::class.java.simpleName, "Connected successfully")

                //subscribe to the topic "careai/mobile/app/response"
                client.subscribeWith()
                    .topicFilter(CAREAI_RESPONSE_TOPIC)
                    .send()

                client.subscribeWith()
                    .topicFilter(CAREAI_ERROR_TOPIC)
                    .send()

                //listen to subscribed topics
                client.toAsync().publishes(MqttGlobalPublishFilter.SUBSCRIBED) { publish ->
                    val topic = publish.topic
                    val payload = UTF_8.decode(publish.payload.get())
                    Log.d(MainActivity::class.java.simpleName,
                        "Topic: ${topic}\n" +
                                "Payload: $payload"
                    )
                }

                //publish messages to the topic "careai/mobile/app/request"
                val message = JSONObject().apply {
                    put("command", "start")
                }.toString()
                client.publishWith()
                    .topic(CAREAI_REQUEST_TOPIC)
                    .payload(UTF_8.encode(message))
                    .send()
            }
    }

    companion object {
        private const val IDENTIFIER = "client-android"
        private const val HOST = "542738a5d37a4143ac49331d987c7172.s1.eu.hivemq.cloud"
        private const val USER = "care.ai"
        private const val PASS = "Mobile@ppT3st"

        private const val CAREAI_REQUEST_TOPIC = "careai/mobile/app/request"
        private const val CAREAI_RESPONSE_TOPIC = "careai/mobile/app/response"
        private const val CAREAI_ERROR_TOPIC = "careai/mobile/app/error"
    }
}